package ej2;

import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ej2 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");
        
        Bson projection = new Document("_id",1);
        Bson sort = new Document("pop", -1);
        
        coll.find().projection(projection).sort(sort).limit(10)
        	.forEach((Document doc) -> System.out.println(doc.toJson()) );

	}

}
