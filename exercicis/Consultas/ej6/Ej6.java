package ej6;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Ej6 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");
        
        Bson filter = new Document("city", "KANSAS CITY");
        int total=0;
        try (MongoCursor<Document> mc = coll.find(filter).iterator()) {
        	while (mc.hasNext()) {
	        	Document doc = mc.next();
	        	System.out.println("Cp: " + doc.getString("_id"));
	        	System.out.println("Poblacion: "+ doc.getString("pop"));
	        	total += Integer.valueOf(doc.getString("pop"));
	        	System.out.println("---------------------------");
        	}
        	System.out.println("---------------------------");
        	System.out.println("---------------------------");
        	System.out.println("Poblacion total: "+total);
        }
        mongoClient.close();
		
	}
}
