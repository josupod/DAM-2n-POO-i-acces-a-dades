package ej4;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.lt;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.sun.net.httpserver.Filter;
import com.sun.org.apache.xpath.internal.operations.Gt;

public class Ej4 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");
        
        Bson projection = new Document("city",1);
        Bson filter = and(lt("pop",50));
        
        coll.find().projection(projection)
        	.forEach((Document doc) -> System.out.println(doc.toJson()) );

	}

}
