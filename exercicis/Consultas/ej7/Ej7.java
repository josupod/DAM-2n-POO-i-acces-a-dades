package ej7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;import javax.swing.Spring;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Ej7 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");
        List<Springfield> springfields = new ArrayList<>();
        Set<String> set= new HashSet<String>();
      
        long cont=coll.count(new Document("city","SPRINGFIELD"));
        System.out.println("Total="+cont);
        coll.find(new Document("city","SPRINGFIELD")).forEach((Document doc) ->{
        	System.out.println(doc.get("loc"));
        		springfields.add(new Springfield(doc.getString("_id"),(List<String>)doc.get("loc"),doc.getString("state")));
        });
        for(Springfield s:springfields){
        	System.out.println("State: "+s.getState());
        	System.out.println("State: "+s.getLoc().toString());
        	System.out.println("------------------------");
        }

	}

}
