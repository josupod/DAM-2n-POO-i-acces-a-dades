package ej7;

import java.util.ArrayList;
import java.util.List;

public class Springfield {
	private Integer cp;
	private List<Double> loc = new ArrayList<>();
	private String state;
	
	public Springfield(String cp, List<String> loc, String state) {
		
		this.cp=Integer.parseInt(cp);
		this.loc.add(Double.parseDouble(loc.get(0)));
		this.loc.add(Double.parseDouble(loc.get(1)));
		this.state=state;
		
	}
	
	public int getCp() {
		return cp;
	}

	public void setCp(int cp) {
		this.cp = cp;
	}

	public List<Double> getLoc() {
		return loc;
	}

	public void setLoc(List<Double> loc) {
		this.loc = loc;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
