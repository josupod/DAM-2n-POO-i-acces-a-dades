package ej5;

import static com.mongodb.client.model.Sorts.orderBy;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;



public class Ej5 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");
        
        Bson idsmall = orderBy(Sorts.ascending("_id"));
        Bson idbig = orderBy(Sorts.descending("_id"));
        List<String> states = new ArrayList<>();

        coll.distinct("state", String.class).forEach((String doc) -> states.add(doc));
        for (String s : states) {
        	System.out.println("Estat: " + s);
        	System.out.println("..................................................");
        	coll.find(new Document("state", s)).sort(idsmall).limit(1)
        			  .forEach((Document doc) -> 
        			  System.out.println("Codigo mas peque�o: "
					        			  +doc.getString("_id")+"\nCiudad: "
					        			  +doc.getString("city")));
        	System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        	coll.find(new Document("state", s)).sort(idbig).limit(1)
                      .forEach((Document doc) ->
                      System.out.println("Codigo mas grande: "
                    		  			  +doc.getString("_id")+"\nCiudad: "
                    		  			  +doc.getString("city")));
        	System.out.println("--------------------------------------------------");
        	System.out.println("--------------------------------------------------");
        	System.out.println("--------------------------------------------------");
        	System.out.println("--------------------------------------------------");
        }
        
        mongoClient.close();
	}
}

