package ej3;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ej3 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");

        Bson projection = new Document("city",1).append("loc", 1);
        Bson orderBy = new Document("loc.1", 1);
        
        long count=(coll.count()/2);
        
        coll.find().projection(projection).sort(orderBy).skip((int)count).limit(1)
    		.forEach((Document doc) -> System.out.println(doc.toJson()) );

	}

}
