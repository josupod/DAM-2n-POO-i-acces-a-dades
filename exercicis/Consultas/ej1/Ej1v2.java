package ej1;

import java.util.Scanner;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;

public class Ej1v2 {
	public final static Scanner SCAN = new Scanner(System.in);
	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("EEUU");
        MongoCollection<Document> coll = db.getCollection("zips");
        
        System.out.println("Introduce el pais a buscar: ");
        String pais = SCAN.nextLine();
        Bson projection = new Document("_id",1);
        Bson filter = new Document("city",pais);
        coll.find(filter).projection(projection).forEach((Document doc) -> System.out.println(doc.toJson()) );
        System.out.println();
        

	}

}
