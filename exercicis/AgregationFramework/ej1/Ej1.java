package ej1;

import java.util.Arrays;
import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Ej1 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("students");
        MongoCollection<Document> coll = db.getCollection("students");
        coll.aggregate(Arrays.asList(
        		unwind("$scores"),
        		match(eq("scores.type","exam")),
        		group("$name", avg("nota", "$scores.score")),
        		match(gte("nota", 40))
        		)).forEach((Document doc) -> {
        			System.out.println(doc.getString("_id")+" = " + doc.getDouble("nota"));
        		});
        mongoClient.close();
	}

}
