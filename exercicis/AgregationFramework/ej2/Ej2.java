package ej2;

import static com.mongodb.client.model.Aggregates.unwind;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;

public class Ej2 {
	private static int last = -1;
	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("students");
        MongoCollection<Document> coll = db.getCollection("students");
        
        coll.aggregate(Arrays.asList(
    			unwind("$scores"),
    			group(new Document("id","$_id").append("name", "$name").append("type", "$scores.type"), sum("quantitat", 1)),
    			sort(Sorts.ascending("_id.name", "_id.id"))
    		)).forEach((Document doc) -> {
    			Document id = doc.get("_id", Document.class);
    			if (last!=id.getInteger("id")) {
    				last = id.getInteger("id");
    				System.out.println("Alumne "+id.getInteger("id")+" "+id.getString("name"));
    			}
    			System.out.println("  Tipus: "+id.getString("type")+"  Quantitat: "+doc.getInteger("quantitat"));
    		});
    		mongoClient.close();

	}

}
