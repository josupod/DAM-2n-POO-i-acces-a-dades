package ej4;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Sorts.ascending;

public class Ej4aggregate {
	
	private static Student student;
	
	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("students");
        MongoCollection<Document> coll = db.getCollection("students");
        
        coll.aggregate(Arrays.asList(
    			unwind("$scores"),
    			group(new Document("id","_id").append("name", "$name").append("type", "$scores.type")
    				  ,avg("score", "$score.scores"))
    			)).forEach((Document doc) -> {
    					Document dStudent = doc.get("_id", Document.class);
    					if (student==null || student.getId() != dStudent.getInteger("id")) {
    						if (student!=null)
    							System.out.println(student.toString());
    						student = new Student(dStudent.getInteger("id"), dStudent.getString("name"));
    					}
    					if(dStudent.getString("type")=="quiz"){
    						student.setExam(doc.getDouble("score"));
    					}else if(dStudent.getString("type")=="exam"){
    						student.setExam(doc.getDouble("exam"));
    					}else if(dStudent.getString("type")=="homework"){
    						student.setExam(doc.getDouble("exam"));
    					}
    				});
        mongoClient.close();

	}

}
