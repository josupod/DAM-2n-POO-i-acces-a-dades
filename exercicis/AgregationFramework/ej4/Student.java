package ej4;

public class Student {
	
	private int id;
	private String name;
	private Double homework;
	private Double exam;
	private Double quiz;
	
	public Student(int id, String name){
		this.name=name;
		this.id=id;
	}
	

	@Override
	public String toString() {
		return this.name+" -> Nota final= "+getNotaFinal();
	}
	
	private Double getNotaFinal(){
		Double  nota;
		nota=(this.exam*0.6)+(this.quiz*0.3)+(this.homework*0.1);
		return nota;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getHomework() {
		return homework;
	}

	public void setHomework(Double homework) {
		this.homework = homework;
	}

	public Double getExam() {
		return exam;
	}

	public void setExam(Double exam) {
		this.exam = exam;
	}

	public Double getQuiz() {
		return quiz;
	}

	public void setQuiz(Double quiz) {
		this.quiz = quiz;
	}
	
	
	
}
