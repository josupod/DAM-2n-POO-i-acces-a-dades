package ej3;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.unwind;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.unwind;

public class Ej3 {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("students");
        MongoCollection<Document> coll = db.getCollection("students");
        
        coll.aggregate(Arrays.asList(
    			unwind("$scores"),
    			group("$scores.type", avg("avg","$scores.score"))
    	)).forEach((Document doc) -> {
    		System.out.println(doc.getString("_id")+" Nota media: "+doc.getDouble("avg"));
    	});
        mongoClient.close();
        
	}

}
