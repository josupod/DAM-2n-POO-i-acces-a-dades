package fungus;

public abstract class Fungus {
	private final Colonia colonia;
	private int fila;
	private int col;
	
	public Fungus(Colonia colonia) {
		this.colonia = colonia;
	}
	
	public abstract char getChar();
	
	public int getFila() {
		return fila;
	}
	
	public void setFila(int fila) {
		this.fila = fila;
	}
	
	public int getCol() {
		return col;
	}
	
	public void setCol(int col) {
		this.col = col;
	}
	
	public Colonia getColonia() {
		return colonia;
	}
	
	public abstract void creix(Cultiu cultiu);
	
	protected boolean posa(Cultiu cultiu, int novaFila, int novaCol) {
		boolean potPosar = false;
		if (cultiu.esDins(novaFila, novaCol)) {
			Fungus f = cultiu.getFungus(novaFila, novaCol);
			if (f == null || (f instanceof Arrencable && this instanceof Arrencador)) {
				cultiu.setFungus(this, novaFila, novaCol);
				potPosar = true;
			}
		}
		return potPosar;
	}
}
