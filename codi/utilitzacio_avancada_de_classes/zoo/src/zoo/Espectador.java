package zoo;

public class Espectador implements Esser {

	public String accio(Zoo z) {
		String s;
		Animal a = z.mostraAnimal();
		if (a instanceof Vaca)
			s = "Un espectador mira una vaca";
		else if (a instanceof Cocodril)
			s = "Un espectador mira a un perillós cocodril";
		else
			s = "Un espectador no sap a on mirar perquè no troba animals";
		return s;
	}

}