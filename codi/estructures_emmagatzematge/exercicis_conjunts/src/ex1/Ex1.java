package ex1;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Ex1 {

	public static void main(String[] args) {
		String cadena;
		StringBuffer buf = new StringBuffer(); // També es pot fer amb String
		String[] paraules;
		char ch;
		Set<String> conjunt = new HashSet<String>();
		int i;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Escriu un text: ");
		cadena = scanner.nextLine();
		cadena = cadena.toUpperCase();
		// Eliminem tots els caràcters que no ens interessen
		for (i = 0; i < cadena.length(); i++) {
			ch = cadena.charAt(i);
			if (Character.isLetter(ch) || ch == ' ')
				buf.append(ch);
		}
		cadena = buf.toString();
		// Afegim totes les paraules a un conjunt
		paraules = cadena.split(" ");
		for (String paraula : paraules)
			conjunt.add(paraula);
		// Per cada paraula del conjunt mirem quants cops apareix
		for (String paraula : conjunt) {
			i = comptaOcurrencies(paraules, paraula);
			System.out.println(paraula + " (" + i + ")");
		}
		scanner.close();
	}

	public static int comptaOcurrencies(String[] paraules, String paraula) {
		int comptador = 0;

		for (String p : paraules) {
			if (paraula.equals(p))
				comptador++;
		}
		return comptador;
	}

}
