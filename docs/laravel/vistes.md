## Vistes

Laravel ens permet organitzar les nostres vistes HTML en petits segments que
després podem composar per obtenir les vistes més complexes. A més, ens
ofereix un llenguatge de plantilles anomenat *Blade* amb el qual podem
definir el comportament de les parts variables d'una vista.

La idea d'utilitzar un llenguatge de plantilles per a la vistes és minimitzar
l'ús de PHP en aquesta part del codi, garantint així que la lògica de
l'aplicació d'implementa als controladors. Idealment, una vista ha de rebre
tota la informació necessària per poder-se mostrar i no ha de contenir
codi a part del codi necessari per mostrar correctament les dades.

Les plantilles Blade també ens permeten incloure plantilles dins d'altres
plantilles, segmentant les vistes i fent que el seu codi tingui un
manteniment més fàcil.

### Creació de vistes

Les vistes típicament es crearan en els controladors o, en casos molt senzills,
directament al fitxer d'enrutament `routes.php`. El codi de les vistes el
guardarem al directori `resources/views` i, si contenen parts en *blade* els
posarem l'extensió `.blade.php`.

Per crear una vista utilitzarem la funció global *view* que rep dos
paràmetres. El primer és el nom de la vista a crear i el segon és un array
associatiu amb la informació que volem posar a disposició de la vista.

Per exemple, aquest codi:

```php5
Route::get('/', function () {
    return view('greeting', ['name' => 'Maria']);
    // Això és equivalent a la sentència anterior:
    // return view('greeting')->with('name', 'Maria');
});
```

crea la vista anomenada *greeting* passant-li el paràmetre *name* amb valor
*Maria*.

El codi de la vista *greeting* podria ser similar a aquest:

```html
<html>
    <body>
        <h1>Hola, {{ $name }}.</h1>
    </body>
</html>
```

Aquest fitxer seria `resources/views/greeting.blade.php`. El codi `{{ }}` és
part del llenguatge *blade* i permet mostrar el contingut d'una variable. És
aproximadament equivalent a `<?php echo $name; ?>` amb alguna validació de
les dades addicional per qüestions de seguretat.

Si no estem segurs que la variable *$name* estigui definida, podem assignar-li
un valor per defecte:

```html
<html>
    <body>
        <h1>Hola, {{ $name or 'usuari desconegut' }}.</h1>
    </body>
</html>
```

Aquesta expressió és aproximadament equivalent al codi PHP:

```php5
echo isset($name) ? $name : 'usuari desconegut';
```

Si creem una estructura de directoris dins de `resources/views` podrem accedir
a les vistes utilitzant el punt com a separador. Per exemple,
`view('base.greeting')` es referiria a la vista `greeting` dins del directori
`base`.

### Estructures de control

El llenguatge *blade* ens permet utilitzar directament les estructures de
control habituals: *if*, *while*, etc.

#### Condicionals

```html
@if (count($gardens) === 1)
  <p>Tens un jardí.</p>
@elseif (count($gardens) > 1)
  <p>Tens diversos jardins.</p>
@else
  <p>No tens cap jardí.</p>
@endif
```

#### Bucles

```html
@for ($i = 0; $i < 10; $i++)
  <p>Valor de i: {{ $i }}</p>
@endfor

@foreach ($gardens as $garden)
    <p>Jardí: {{ $garden->name }}</p>
@endforeach

@forelse ($gardens as $garden)
    <li>{{ $garden->name }}</li>
@empty
    <p>No tens cap jardí</p>
@endforelse
```

### Esquemes de plantilles

Una de les parts més potents del Blade és poder definir esquemes de plantilles
reutilitzables.

Un esquema (*layout*) no és més que un tros de codi HTML en què s'hi han situat
una sèrie d'etiquetes que en permeten la seva ampliació.

Les principals etiquetes que utilitzem són `@section` i `@yield`. `@section`
simplement defineix una part del codi com una secció concreta. Això ens permet
que altres plantilles localitzin la secció que hem definit i, per exemple,
l'ampliïn amb el seu propi contingut. En canvi, `@yield` defineix una posició
on ha d'anar-hi un contingut, que les plantilles que utilitzen l'esquema
s'han d'encarregar de completar.

Podríem fer una analogia entre mètodes i etiquetes *Blade*: `@section`
defineix un mètode normal, que les classes derivades poden sobreescriure o
ampliar, mentre que `@yield` defineix un mètode abstracte, que les classes
derivades esta obligades a implementar.

Mirem tot això amb un exemple:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="An artificial gardening game">
    <meta name="author" content="Joan Queralt">
    <meta name="author" content="Eivipop">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>
        @section('title')
        aGarden - the artificial gardening game
        @show
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @section('styles')
    {{-- CSS are placed here --}}
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-theme.min.css') }}
    {{ HTML::style('css/agarden.css') }}
    @show

  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">aGarden</a>
        </div>
        <div class="collapse navbar-collapse">
          @include('navbar_menu')
          @include('login_form')
        </div><!--/.nav-collapse -->
      </div>
    </div>
    @yield('navbar')
    <div class="container">
      @include('messages')
      @yield('content')
    </div><!-- /.container -->

    <footer><div class="container">
        <p>Alguna cosa aquí</p>
    </div></footer>

    @section('scripts')
    {{-- Scripts are placed here --}}
    {{ HTML::script('js/jquery-1.11.0.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/jquery.validate.min.js') }}
    {{ HTML::script('js/agarden.js') }}
    @show

  </body>
</html>
```

En aquest exemple podem veure l'ús de diverses característiques de *Blade*.
Analitzem-ho part per part:

- Al principi tenim la secció *title*. Definir aquesta secció és útil perquè
qualsevol vista derivada podrà modificar el text del títol, o ampliar-lo. Es
pot veure que la directiva `@show` finalitza la definició de la secció, fent
que es processi la part de dins i es mostri.

- A continuació tenim la secció *styles*. Aquí s'han afegit alguns fulls
d'estil que utilitza tota l'aplicació, però si alguna pàgina necessita algun
full d'estil més, pot ampliar aquesta secció amb altres *css*. De passada,
veiem com fer-ho per generar enllaços a elements que tenim guardats al
directori *public* de la nostra aplicació. En aquest directori hi guardarem
tot allò que s'hagi d'enviar als client sense fer-ne cap modificació, com
imatges, fulls d'estil, o codi *javascript*.

- Més endavant tenim dues seccions `@include`: *navbar_menu* i *login_form*.
La directiva `@include` és senzilla: simplement se cerca la vista amb el nom
indicat i el seu codi s'inclou en el punt on hi ha l'*include*. És útil
sobretot per no generar fitxers HTML molt llargs, i per reaprofitar parts que
poden aparèixer a diverses pàgines. Més endavant donarem un cop d'ull al
fitxer *login_form.blade.php* per veure com encaixa aquí.

- Després ens trobem amb la directiva `@yield('navbar')`. Aquesta secció
permet que cada plantilla derivada d'aquesta defineixi el seu propi menú. Al
contrari del menú anterior, la idea d'aquesta secció és que apareixi com un
menú en una barra lateral.

- La secció *messages* s'utilitza per mostrar missatges a l'usuari. Després
analitzarem el seu codi.

- Després afegim la secció *content*, en què hi haurà el contingut principal
de la pàgina.

- Finalment, la secció *script* ens permet afegir codi *javascript* a algunes
pàgines que el necessitin, a més del codi que s'utilitza en general a tota
l'aplicació.

Mirem ara el contingut de la vista *login_form.blade.php*:

```html
<div class='nav navbar-nav'>
@if (Auth::check())
  <p class='navbar-text'>Welcome {{ Auth::user()->username }}</p>
@else
  <p class='navbar-text'>Welcome guest!</p>
@endif
</div>
<div class='nav navbar-nav navbar-right'>
  <ul class="nav navbar-nav">
@if (Auth::check())
    <li id="menuLogout">
      <a href='{{ action("UserController@handleLogout") }}' class='navbar-link'>Log out</a>
    </li>
@else
    <li class="dropdown" id="menuLogin">
      <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Login <b class="caret"></b></a>
      <div class="dropdown-menu">
        {{ Form::open(array('id' => 'login-form', 'class' => 'form', 'action' => 'UserController@handleLogin')) }}
          <label>Login</label>
          <div class='form-group'>
            <label class='sr-only' for='username'>Username</label>
            <input type='text' class='form-control' name='username' placeholder='Username'>
          </div>
          <div class="form-group">
            <label class='sr-only' for='password'>Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary">Login</button>
        </form>
        <a role="button" href="{{ URL::secure('password/remind') }}">Forgot username or password?</a>
      </div>
    </li>
    <li id="menuRegister">
      <a class='navbar-link' href="{{ URL::secure('register') }}">Register</a>
    </li>
@endif
  </ul>
</div>
```

En aquest codi veiem com es completa la part corresponent afegint un
formulari que permet introduir les dades d'entrada, en cas que l'usuari no
s'hagi autenticat encara, o un botó que li permet desconnectar-se si ja s'ha
autenticat.

Vegem ara una vista que derivi de *master*, per exemple, la vista que permet
visualitza un jardí:

```html
@extends('master')

@section('styles')
@parent
{{ HTML::style('css/bootstrap-progressbar-3.1.1.min.css') }}
{{ HTML::style('css/gardenview.css') }}
@stop

@section('navbar')
  @include('garden.sidebar')
@stop

@section('content')
<div class="row">
  @include('garden.actionpoints')
  <div class="col-sm-6 col-md-5 col-md-offset-3">
    Temperature: {{ $garden->climate->temp() }}, rain: {{ $garden->climate->rain() }}.
    It is {{ $garden->climate->season() }}, the climate is {{ $garden->climate->name }} - @include('garden.ecopoints')
  </div>
</div>

<div id="cells" class="row">
  @foreach ($garden->cells as $cell)
    @include('garden.cell')
  @endforeach
  @include('garden.nextcell')
</div>

<div class="row">
  <div class="col-md-12">
    <div>Seeds</div>
    @foreach ($garden->seeds as $seed)
      @include ('garden.seed')
    @endforeach
  </div>
</div>
@stop

@section('scripts')
@parent
{{ HTML::script('js/bootstrap-progressbar.min.js') }}
{{ HTML::script('js/easeljs-NEXT.min.js') }}
{{ HTML::script('js/preloadjs-0.4.1.min.js') }}
{{ HTML::script('js/gardenview.js') }}
@stop {{-- section scripts --}}
```

Al principi de tot hem utilitzat `@extends('master')` per indicar que aquesta
vista utilitza la plantilla que hem definit abans a *master*.

Després s'amplia la secció d'estils amb dos *css* que s'utilitzen en aquesta
vista. S'ha utilitzat `@parent` per incloure el contingut original de la
secció. Al final hem fet alguna cosa similar amb la secció *scripts*. En
canvi, hem optat per no modificar la secció *title*, que mantindrà
exactament el contingut del *master* original.

Es pot veure com hem afegit un menú lateral, afegint la secció
*navbar* amb una nova vista que conté aquest menú, *sidebar* dins del
directori *garden*.

A la secció de contingut s'inclou la visualització del jardí primer i després
la visualització de les llavors disponibles. Cada una d'aquestes parts inclouen
altres vistes.
